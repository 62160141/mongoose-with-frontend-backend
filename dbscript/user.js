const mongoose = require('mongoose')
const User = require('../models/User')
mongoose.connect('mongodb://localhost:27017/example')
const { ROLE } = require('../constant.js')

async function clearUser () {
  await User.deleteMany({})
}

async function main () {
  await clearUser()

  const user = new User({ username: 'User@mail.com', password: 'password', roles: [ROLE.USER] })
  user.save()
  const Admin = new User({ username: 'Admin@mail.com', password: 'password', roles: [ROLE.ADMIN, ROLE.USER] })
  Admin.save()
}

main().then(function () {
  console.log('Finish')
})
